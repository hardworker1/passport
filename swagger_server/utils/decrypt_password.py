from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5 as pkcs1
import base64

def decrypt_message(message: str):
    rsa_private_key = ''
    with open('C:/Users/26578/Desktop/hippoReaderProject/passport/swagger_server/utils/private.pem', 'r') as f:
        rsa_private_key = f.read()
    rsa_key = RSA.importKey(rsa_private_key)
    cipher = pkcs1.new(rsa_key)
    text = cipher.decrypt(base64.b64decode(message), b'error')
    return text.decode()
