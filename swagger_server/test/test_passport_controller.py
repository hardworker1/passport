# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.create_request import CreateRequest  # noqa: E501
from swagger_server.models.create_response import CreateResponse  # noqa: E501
from swagger_server.models.retrieve_response import RetrieveResponse  # noqa: E501
from swagger_server.test import BaseTestCase


class TestPassportController(BaseTestCase):
    """PassportController integration test stubs"""

    def test_get_verify_code(self):
        """Test case for get_verify_code

        
        """
        query_string = [('Phone', 'Phone_example')]
        headers = [('Authorization', 'Authorization_example')]
        response = self.client.open(
            '/passport/v1/items',
            method='GET',
            headers=headers,
            content_type='application/json',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_user_register(self):
        """Test case for user_register

        
        """
        create_rquest = CreateRequest()
        headers = [('Authorization', 'Authorization_example')]
        response = self.client.open(
            '/passport/v1/item',
            method='POST',
            data=json.dumps(create_rquest),
            headers=headers,
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
