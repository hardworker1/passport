import connexion
import six
from swagger_server.utils.zhenzismsclient import ZhenziSmsClient

from swagger_server.models.create_request import CreateRequest  # noqa: E501
from swagger_server.models.create_response import CreateResponse  # noqa: E501
from swagger_server.models.retrieve_response import RetrieveResponse  # noqa: E501
from swagger_server import util
from swagger_server.constants import *
from swagger_server.controllers.proc_verify_code import ProcVerifyCode
from swagger_server.controllers.user_register import RegisterUser
from swagger_server.models.login_params import LoginParams
from swagger_server.controllers.login_check import LoginCheck
from swagger_server.models.reset_body import ResetBody  # noqa: E501
from swagger_server.controllers.reset_password import ResetPassword


def get_verify_code(Phone, Authorization=None):  # noqa: E501
    """get_verify_code
     # noqa: E501
    :param Authorization: 
    :type Authorization: str
    :param Phone: 
    :type Phone: str
    :rtype: RetrieveResponse
    """
    authorization = connexion.request.headers.get("Authorization")
    if authorization != PASSPORT_AUTHORIZATION:
        return {"error": "You have no authority."}
    response = ProcVerifyCode(Phone).build_response()
    return response


def user_register(create_request, Authorization=None):  # noqa: E501
    """user_register

     # noqa: E501

    :param Authorization: 
    :type Authorization: str
    :param create_rquest: 
    :type create_rquest: dict | bytes

    :rtype: CreateResponse
    """
    authorization = connexion.request.headers.get("Authorization")
    if authorization != PASSPORT_AUTHORIZATION:
        return {"error": "You have no authority."}
    if not connexion.request.is_json:
        return {"error": "request parameter invalid"}
    create_request = CreateRequest.from_dict(connexion.request.get_json())  # noqa: E501
    response = RegisterUser(create_request).build_response()
    return response


def login_check(login_params, Authorization=None):  # noqa: E501
    """login_check
     # noqa: E501
    :param Authorization:
    :type Authorization: str
    :param login_params:
    :type login_params: dict | bytes
    :rtype: CreateResponse
    """
    authorization = connexion.request.headers.get('Authorization')
    if authorization != PASSPORT_AUTHORIZATION:
        return {'error': 'You have no authority.'}
    if connexion.request.is_json:
        login_params = LoginParams.from_dict(connexion.request.get_json())  # noqa: E501
        return LoginCheck(login_params).build_response()
    return {"error": "参数无效"}

def reset_passport(reset_body, Authorization=None):  # noqa: E501
    """reset_passport
     # noqa: E501
    :param Authorization:
    :type Authorization: str
    :param reset_body:
    :type reset_body: dict | bytes
    :rtype: DefaultResponse
    """
    authorization = connexion.request.headers.get('Authorization')
    if authorization != PASSPORT_AUTHORIZATION:
        return {'error': 'You have no authority.'}
    if connexion.request.is_json:
        reset_body = ResetBody.from_dict(connexion.request.get_json())  # noqa: E501
        return ResetPassword(reset_body).build_response()
    return {"error": "参数无效"}
