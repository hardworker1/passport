from swagger_server.utils.oms_client import OmsClient
from cryptography.fernet import Fernet
import email
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.header import Header
from jinja2 import PackageLoader, Environment
from swagger_server.constants import *
import uuid
from swagger_server.utils.decrypt_password import decrypt_message


class RegisterUser:
    def __init__(self, create_request):
        self.user_info = create_request.data
        self.user_id = ''

    def build_response(self):
        status = self.send_user_email()
        if not status:
            return {"error": "邮箱错误，请检查邮箱是否输入错误。"}
        status, error_info = self.memory_user_info()
        if not status:
            return error_info
        return {"data": {"userId": self.user_id}}

    def memory_user_info(self):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_USER_PROFILE)
        try:
            data_key = "regex::^.*#{}#.*$".format(self.user_info.email)
            result = oms_client.get_first_value(data_key)
            if result:
                return False, {"error": "邮箱已注册."}
        except:
            return False, {"error": "用户信息存入失败，请稍后再试。"}
        try:
            data_key = "regex::^.*#{}#.*$".format(self.user_info.phone)
            result = oms_client.get_first_value(data_key)
            if result:
                return False, {"error": "手机号已注册."}
        except:
            return False, {"error": "用户信息存入失败，请稍后再试。"}
        user_id = str(uuid.uuid4())
        self.user_id = user_id
        decr_password = decrypt_message(self.user_info.password)
        new_password = self.password_encryption(decr_password)
        oms_client = OmsClient(OMS_CREATE_URL, OMS_NAMESPACE_USER_PROFILE)
        try:
            data_key = user_id + "#" + self.user_info.phone + "#" + self.user_info.email + "#" + self.user_info.user_name
            data_value = {
                "userId": user_id,
                "userName": self.user_info.user_name,
                "phone": self.user_info.phone,
                "email": self.user_info.email,
                "password": new_password
            }
            oms_client.create(data_key, data_value)
        except:
            return False, {"error": "用户信息存入失败，请稍后再试。"}
        return True, None

    def password_encryption(self, password):
        new_password = Fernet(SECRET_KEY).encrypt(password.encode()).decode()
        return new_password

    def send_user_email(self):
        env = Environment(loader=PackageLoader('swagger_server', 'template'), trim_blocks=True)
        mail_dict = {"loginUrl": HIPPO_READER_URL,
                     "nick_name": self.user_info.user_name,
                     "phone": self.user_info.phone,
                     "email": self.user_info.email}
        template = env.get_template('welcome_email.html')
        mail_body = template.render(mail_dict=mail_dict)
        message = MIMEMultipart('alternative')
        message['From'] = "%s <%s>" % (Header(EMAIL_SEND_NAME).encode(), EMAIL_SENDER)
        message["To"] = Header(self.user_info.email, 'UTF-8')
        message["Reply-to"] = EMAIL_SENDER
        message['Subject'] = Header(EMAIL_SUBJECT, 'utf-8')
        message['Message-id'] = email.utils.make_msgid()
        message['Date'] = email.utils.formatdate()
        mail_html = MIMEText(mail_body, 'html', 'utf-8')
        message.attach(mail_html)
        try:
            with smtplib.SMTP_SSL(QQ_EMAIL_URL, QQ_EMAIL_CODE) as smtp_ssl:
                smtp_ssl.login(QQ_EMAIL_ACCOUNT, QQ_EMAIL_PASSWORD)
                smtp_ssl.sendmail(QQ_EMAIL_ACCOUNT, self.user_info.email, message.as_string())
        except:
            return False
        return True



