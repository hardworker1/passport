from swagger_server.utils.oms_client import OmsClient
from cryptography.fernet import Fernet
from swagger_server.constants import *
from swagger_server.utils.decrypt_password import decrypt_message
import base64

class ResetPassword:
    def __init__(self, reset_params):
        self.phone = reset_params.phone
        self.newpassword = reset_params.newpassword

    def build_response(self):
        decr_password = decrypt_message(self.newpassword)
        password_encode = Fernet(SECRET_KEY).encrypt(str.encode(decr_password)).decode()
        status, error_info = self.reset_password(password_encode)
        if not status:
            return error_info
        return {"data": "Success"}

    def reset_password(self, new_password):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_USER_PROFILE)
        try:
            data_key = "regex::^.*#{}#.*$".format(self.phone)
            result = oms_client.get_first_value(data_key)
            if not result:
                return False, {"error": "手机号未注册."}
            data_value = result.get("dataValue")
            data_value["password"] = new_password
            data_key = result.get("dataKey")
            oms_client.update(data_key, data_value)
        except:
            return False, {"error": "更新密码失败，请重新操作。"}
        return True, None


