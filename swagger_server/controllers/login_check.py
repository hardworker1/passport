from swagger_server.utils.oms_client import OmsClient
from swagger_server.constants import *
from cryptography.fernet import Fernet
from swagger_server.utils.decrypt_password import decrypt_message


class LoginCheck:
    def __init__(self, params):
        self.account = params.account
        self.psdCode = params.psd_code
        self.loginType = params.login_type

    def build_response(self):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_USER_PROFILE)
        data_key = "regex::^.*#{}#.*".format(self.account)
        try:
            result = oms_client.get_first_value(data_key)
        except:
            return {"data": {"error": "获取信息失败，请重新操作"}}
        if not result:
            return {"data": {"error": "用户名不存在， 请检擦账号是否输入错误"}}
        data_value = result.get("dataValue")
        password = data_value.get('password')
        password_encode = Fernet(SECRET_KEY).decrypt(str.encode(password)).decode()
        decr_password = decrypt_message(self.psdCode)
        if password_encode == decr_password:
            return {"data": {"userId": data_value.get("userId")}}
        return {"data": {"error": "密码错误"}}