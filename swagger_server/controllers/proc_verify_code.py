import random
from swagger_server.utils.zhenzismsclient import ZhenziSmsClient
from swagger_server.constants import *
import json

class ProcVerifyCode:
    def __init__(self, phone):
        self.phone = phone

    def build_response(self):
        verify_code = self.build_verify_code()
        return self.send_verify_code(verify_code)

    def build_verify_code(self):
        verify_code = ''
        for i in range(6):
            verify_code += str(random.randint(0, 9))
        return verify_code

    def send_verify_code(self, verify_code):
        smsclient = ZhenziSmsClient(ZHENZI_API_URL, ZHENZI_APP_ID, ZHENZI_APP_SECRET)
        params = {
            "templateId": ZHENZI_TEMPLATE_ID,
            "number": self.phone,
            "templateParams": [verify_code, '30分钟']
        }
        result_str = smsclient.send(params)
        result = json.loads(result_str)
        if not result:
            return {"error": "send verify code error"}
        if result.get("code") != 0:
            return {"error": result.get("data")}
        else:
            return {"data": verify_code}

if __name__ == '__main__':
    phone = "13970723540"
    print(ProcVerifyCode(phone).build_response())
