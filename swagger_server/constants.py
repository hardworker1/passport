import socket
local_ip = socket.gethostbyname(socket.gethostname())

OMS_PORT = '8081'
HIPPO_PORT = '8080'
HIPPO_ADMIN_PORT = '4444'
HIPPO_UX_PORT = '8082'
PASSPORT_PORT = '8084'

OMS_URL = "http://" + local_ip + ":" + OMS_PORT + "/oms/v1"
OMS_RETRIEVE_URL = OMS_URL + "/items"
OMS_CREATE_URL = OMS_URL + "/item"
OMS_DELETE_URL = OMS_URL + "/items"
OMS_UPDATE_URL = OMS_URL + "/items"

OMS_AUTHORIZATION = 'Bearer mi-oms'

# oms namespace
OMS_NAMESPACE_USER_PROFILE = 'oms-user-profile'
OMS_NAMESPACE_BOOK_PROFILE = 'book-profile'
OMS_NAMESPACE_BOOK_CONTENT = 'book-content'
OMS_NAMESPACE_USER_READ_HISTORY = 'user-read-history'
OMS_NAMESPACE_USER_BOOK_RACK = 'user-book-rack'

PASSPORT_AUTHORIZATION = "Bearer passport-console"

ZHENZI_APP_ID = '108273'
ZHENZI_APP_SECRET = '97a8b2a2-2c25-4003-9fa0-df178efb57eb'
ZHENZI_TEMPLATE_ID = '3929'
ZHENZI_API_URL = 'https://sms_developer.zhenzikj.com'

# user password secret key
SECRET_KEY = b'Fj2vbISqBeZe_65gGcP6gsLvyOxHPNNel2LNiSvjHAY='

# qq email
QQ_EMAIL_ACCOUNT = "2657841387@qq.com"
QQ_EMAIL_PASSWORD = 'erhletgrmyoyebhi'
EMAIL_SEND_NAME = 'HippoReader'
EMAIL_SENDER = '2657841387@qq.com'
EMAIL_SUBJECT = "感谢您加入HippoReader"
QQ_EMAIL_URL = 'smtp.qq.com'
QQ_EMAIL_CODE = 465

# hippo-reader url
HIPPO_READER_URL = "http://" + local_ip + ":" + HIPPO_PORT

# secret key
publicKey = '''-----BEGIN RSA PUBLIC KEY-----
MBACCQCwVJFzUDTNoQIDAQAB
-----END RSA PUBLIC KEY-----'''

privateKey = '''-----BEGIN RSA PRIVATE KEY-----
MIICYgIBAAKBgQCtMdiVIZ9toP73M/c21ZjOmNlyZcGk4j8MAR7OJHabZl7ShPcb
vCSzkubC++/uLegP43N/xPjJciyCuLkb2tBChx9ISb5d4rEX1WFqpREKgU8Xc8Ka
kiGfgC8Shv6n3zEtd7z8mhGxH7Q3TbbUrhPP/Ji2xOG2lsXEQmrJPAjJmwIDAQAB
AoGBAKv7/p2AyOc9Y1KpWMkr7FTjtbKC13tba84I4sVDsj/4tRqKDTSObUehDeP7
+DLlhTcZ31y2hXLNHOQmeHS8U3ka9NquwEBdeD6wYDGsAR7154Kq8xfBy7yiPYWd
t90adG6FOnDxhCUlsu6z1EgzXH5lAnaafWCdgfnGzYj4v+NJAkUAv4k6aJajBNDK
pJi6zQEp3ImHKMJc0m5yqqJH9pvzC2+XANEyAu8+avLR5uUq1cdGRkCs7Fy7aNPQ
ASpzivlac7w/gV8CPQDnfFNpjW18R767ajSFsJ3tnrf5aygM7yp0LZfZuLal/98M
T6rm5A2xBHh7+oQ6G//2UGoetAdmzhbF9UUCRDeXD5G9brD+HGJrRYQse0bbz5Of
3arhjXxCv1ou5vqBt7TwAnDhmR8XPJ6kEqubO6U2Ljonq+j/2NYNyR0jxAHlOBVH
Aj0AvoJjY4sxgAbLwhI2B5DDaLwnTO4TmBF9LwAW6OhL6XIbb1ZhoIQvgG/deKtE
B07j6SNlMr+XF9SkiWbBAkUAl+llEA1wfp5dGfXbbt8fdhisnaLOX6TlubJr8HHh
GtSCXohlqcLA9Cku5i1Ia8NTkkzWo7fnzW9Dh1tlXqvicJZckU4=
-----END RSA PRIVATE KEY-----'''

